# What's here

* SR.xlsx - Excel spreadsheet with SR genes from rice and Arabidopsis, compiled by April Extrada from [Barta et al](http://www.ncbi.nlm.nih.gov/pubmed/20884799) 