#!/usr/bin/env python

ex="Read GTF, write BED-detail. Summarize data file, print to stderr."


"""
producers:

Araport11
  transcripts named locus_PN
  
CoreClock
  transcripts named locus_XN
  only one - AT5G24470
  APRR5|PRR5
  
Cufflinks
  transcripts named locus_cN
   
CufflinksY
  transcripts named locus_JCN
   
Finaltr
  transcripts named locus_IDN
    
StringTie
  transcripts named locus_JSN
  
StringTieY
  transcripts named locus_sN
  
TAIR10
  transcripts named locus.N
  
where N is an positive integer

feature types: CDS, exon

Only Araport11 has functional annotation - "Note" in
extra feature field.

I'm going to migrate our text annotations over from
Ivory's Araport file. 

"""

import os,sys,re,optparse

# from https://bitbucket.org/lorainelab/genomes_src
import Mapping.FeatureModel as feature
import Utils.General as utils
import Mapping.Parser.Bed as Bed

def readGtfFile(fname=None):
    models = gff2feats(fname)
    return models

def writeBedFile(fname=None,feats=None):
    fh=open(fname,'w')
    writeBed(fh=fh,feats=feats)
    fh.close()

def writeBed(fh=None,feats=None):
    for feat in feats:
        bedline = Bed.feat2bed(feat,'BED14')
        if fh:
            fh.write(bedline)
        else:
            sys.stdout.write(bedline)
    if fh:
        fh.close()

def convert(bed_file=None,
            gtf_file=None):
    feats = readGtfFile(fname=gtf_file)
    writeBedFile(fname=bed_file,feats=feats)


def gff2feats(fname=None):
    """
    Function: read features from AtRTD GTF format file
    Returns : list of CompoundDNASeqFeature objects representing spliced
              mRNA transcripts
    Args    : fname - name of file to read
    """
    lineNumber = 0
    fh = utils.readfile(fname)
    RNAs={}
    subfeatss={}
    key_valss={}
    cds_by_producer={}
    while 1:
        line = fh.readline()
        lineNumber = lineNumber + 1
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        try:
            (seqname,producer,feat_type,start,end,
             ignore1,strand,ignore2,extra_feat)=line.strip().split('\t')
        except ValueError:
            sys.stderr.write("Exception occurred at line %i:\n%s" % (lineNumber,line))
            raise
        start = int(start)-1
        end = int(end)
        length=end-start
        if strand == '+':
            strand = 1
        elif strand == '-':
            strand = -1
        elif strand == '.':
            raise ValueError("Feature has no strand.")
        key_vals=parseKeyVals(extra_feat)
        try:
            display_id=key_vals['transcript_id']
        except KeyError:
            raise KeyError("No transcript_id in extra feature field from line %i: %s"%(lineNumber,line))
        # only two feature types - CDS or exon
        feat = feature.DNASeqFeature(seqname=seqname,start=start,length=length,strand=strand,key_vals=key_vals,feat_type=feat_type)
        if feat_type=='CDS':
            if not cds_by_producer.has_key(display_id):
                cds_by_producer[producer]={display_id:1}
        if not subfeatss.has_key(display_id):
            subfeatss[display_id]=[feat]
            key_valss[display_id]=key_vals
        else:
            subfeatss[display_id].append(feat)
            if not key_valss[display_id]==key_vals:
                raise ValueError("Extra feature field for %s doesn't match previous on line number %i, line %s"%(display_id,lineNumber,line))
    for display_id in subfeatss.keys():
        subfeats=subfeatss[display_id]
        rna=feature.CompoundDNASeqFeature(seqname=subfeats[0].getSeqname(),
                                          strand=subfeats[0].getStrand(),
                                          display_id=display_id,
                                          key_vals=key_valss[display_id],
                                          feat_type='mRNA',
                                          feats=subfeats)
        feats=rna.getSortedFeats()
        rna.setStart(feats[0].getStart())
        rna.setLength(feats[-1].getEnd()-rna.getStart()) # interbase
        if RNAs.has_key(display_id):
            raise ValueError("Duplicate transcript: %s"%display_id)
        else:
           RNAs[display_id]=rna
    sys.stderr.write("producers with cds's: %s\n"%','.join(cds_by_producer.keys()))
    return RNAs.values()

def parseKeyVals(extra_feat):
    """
    Parse extra feature fields. Format looks a little different depending on annotation source. For TAIR10 & Araport probably need to get gene description from oher files
    """
    if not extra_feat.endswith(';'):
        raise ValueError("Unexpected extra feature format.")
    extra_feat=extra_feat[0:-1]
    vals = extra_feat.split('; ')
    key_vals = {}
    for item in vals:
        pair=item.split(' ')
        if not len(pair)==2:
            raise ValueError("GTF line has weird extra feature value: %s."%extra_feat)
        key = pair[0]
        val = pair[1]
        if not val.startswith('"') or not val.endswith('"'):
            raise ValueError("GTF value not enclosed in quotes: %s."%val)
        val=pair[1][1:-1]
        if key_vals.has_key(key):
            raise ValueError("GTF line has two values for same key in extra feat field: %s."%extra_feat) 
        key_vals[key]=val
    return key_vals
            
            
def main(bed_file=None,gtf_file=None):
    convert(bed_file=bed_file,
            gtf_file=gtf_file)

if __name__ == '__main__':
    usage = "%prog "+ex
    parser = optparse.OptionParser(usage)
    parser.add_option("-g","--gtf_file",help="GTF file to convert. Accepts both compressed (.gz) and non-compressed input",dest="gtf_file"),
    parser.add_option("-b","--bed_file",help="BED format file to write",
                      dest="bed_file",default=None)
    (options,args)=parser.parse_args()
    if not options.bed_file or not options.gtf_file:
        parser.error("Bed or GTF files not specified.")
    main(gtf_file=options.gtf_file,
         bed_file=options.bed_file)

