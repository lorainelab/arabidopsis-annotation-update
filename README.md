# Annotation updates for Arabidopsis

This repository contains code to for generating BED-detail files
containing gene description and gene structure annotation files for
visualization of Arabidopsis data in Integrated Genome Browser.

# Repository organization

Many folders contain:

* .Rproj file - an RStudio project file. Open this in RStudio to re-run project code.
* .Rmd files - R Markdown files containing self-documenting workflows.
* .html files - output from running knitr on .Rmd files. 
* results folders - contain output from running code within a module
* data - data files obtained from an external resources, e.g., downloaded from a Web site
* src - R, python, or perl scripts used for simple data processing tasks
* README.md - documentation 

* * *

## Araport

Convert Araport pre-release 11 files to bed-detail. Also see https://www.araport.org/downloads/Araport11_PreRelease_20151006 

* * *

## Arabidopsis

Create gene description file for TAIR10 genes using data downloaded from Web site and curated gene descriptions for SR protein genes and cytokinin-related genes.

Writes Arabidopsis/results/genedescr.tsv.gz.

* * *

## AtRTD

Convert AtRTD2 files to bed-detail. Also see http://biorxiv.org/content/early/2016/05/06/051938

Depends on genedescr.tsv.gz 

* * *

## data 

Contains files downloaded from external Web site or created using desktop programs like Excel

* * * 

# Questions?

Contact:

* Ann Loraine aloraine@uncc.edu
* Ivory Clabaugh Blakley ieclabau@uncc.edu

* * *

# License 

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT