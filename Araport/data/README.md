# What's here

* Araport11_genes.20151006.gff.gz - downloaded from Araport Web site Nov 2015

* Araport11.bed.gz - converted Araport11_genes.20151006.gff.gz to BED-detail format using gff3ToBedDetail.py from https://bitbucket.org/lorainelab/genomes_src

