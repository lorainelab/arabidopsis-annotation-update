---
title: "Add curated descriptions to Araport gene models"
output:
  html_document:
    toc: true
---
```{r echo=F}
descFile = "Tair10.bed" # for consistency in comments throughout markdown
pn=function(x){prettyNum(x, big.mark = ",")} # to make big numbers pretty in comments
```

# Introduction

A new and improved set of models for protein-coding genes in Arabidopsis is now available from Araport.  But we still like the descriptions we have for genes in (`r descFile`).
Here, we transfer the symbols and descriptions from that were created for the Tair10 models to the corresponding genes in the Araport set.

# Method

## Get gene descriptions

Read in the file (`r descFile`) that has the symbols and descriptions that we want to use.

This file comes from the IGBQuickLoad.org subversion repository. A copy of it is here for convenience.   

Use svn to check out the latest version from https://svn.transvar.org/repos/genomes/trunk/pub/quickload/A_thaliana_Jun_2009/. 
```{r}
fname = "data/TAIR10.bed.gz"# "results/TAIR10.bed"
desc=read.delim(fname,as.is=T,quote='',header=F)
bednames = c("chromosome", "chromStart", "chromEnd", 
             "name", "score", "strand", 
             "thickStart", "thickEnd", "itemRGB", 
             "blockCount", "blockSizes", "blockStarts", 
             "symbol", "description")
names(desc)=bednames
# convert gene model ids to locus ids
desc$gene=sapply(strsplit(desc[,4],'\\.'),function(x){x[[1]]})
```

This file includes `r pn(nrow(desc))` rows, describing `r pn(length(unique(desc$gene)))` genes. A single gene may have multiple models and each model is represented by one row in a bed file.

All we need from this is the last two fields (13-symbol and 14-description).  This information should be the same for all transcripts of the same gene.
```{r}
desc = desc[!duplicated(desc$gene),c("gene", "symbol", "description")]
row.names(desc) = desc$gene
```


## Get Araport models

Read in the bed file made from the Araport gene models.
```{r}
fnameAra = "../data/Araport11.bed.gz"
AraBed=read.delim(fnameAra,as.is=T,quote='',header=F)
names(AraBed)=bednames
AraBed$gene=sapply(strsplit(AraBed[,4],'\\.'),function(x){x[[1]]})
```
This file includes `r pn(nrow(AraBed))` rows, describing `r pn(length(unique(AraBed$gene)))` genes.

Many genes exist in both files, but some are in only one or the other.

 * `r pn(length(unique(intersect(desc$gene,AraBed$gene))))` genes are in both files.
 * `r pn(length(unique(setdiff(desc$gene,AraBed$gene))))` are found only in the Tair file.
 * `r pn(length(unique(setdiff(AraBed$gene,desc$gene))))` are found only in the Araport file.

## Transfer descriptions

Make a bed file using the information from the Araport file, BUT use the symbol and description fields from the older Tair file.
```{r}
newBed = AraBed[c(bednames[1:12],"gene")]
newBed = merge(newBed, desc, by="gene", all.x=T)
```

Where there is no symbol for a gene in `r descFile`, use the gene name.
```{r}
noSym = which(is.na(newBed$symbol))
newBed[noSym,"symbol"] = newBed[noSym,"gene"]
```
There were `r pn(length(unique(newBed$gene[noSym])))` genes that did not have a symbol (field 13) in `r descFile`.

Where there is no description from `r descFile`, keep the Araport descriptions.
```{r}
# which rows lack descriptions
noDesc = which(is.na(newBed$description))

# make a table that can use genes as row.names
AraBedU = AraBed[!duplicated(AraBed$gene),]
row.names(AraBedU) = AraBedU$gene

# apply the descriptions from AraBed to newBed
newBed[noDesc,"description"] = AraBedU[newBed$gene[noDesc],"description"]
```
There were `r pn(length(unique(newBed$gene[noDesc])))` genes that did not have a description (field 14) in `r descFile`.

## Review changes

We used the gene name for `r pn(length(unique(newBed$gene[noSym])))` gene from the Araport data that did not appear in `r descFile`. Do these gene names follow use same convention as the older gene models?
```{r}
transcriptNames = newBed$name[noSym]
classic = grep("AT[1-5MC]G[0-9]{5}\\.[0-9]*", transcriptNames)
length(transcriptNames) == length(classic)
```


We kept `r pn(length(unique(newBed$gene[noDesc])))` of the original descriptions. What do these descriptions from Araport look like?
```{r}
dt = table(AraBedU[AraBedU$gene %in% newBed$gene[noDesc],"description"])
dt = dt[order(dt, decreasing = T)]
head(data.frame(occurences=dt))
```

How many of the symbols are different as a result of this transfer?
```{r}
shared=intersect(desc$gene, AraBedU$gene)
df=data.frame(T10=desc[shared,"symbol"], Ara=AraBedU[shared,"symbol"])
df$Ara=as.character(df$Ara)
df$T10=as.character(df$T10)
df$isSame = df$T10==df$Ara
t=table(df$isSame)
```
Transferring the symbols from `r descFile` changed field 13 for `r pn(t["FALSE"])` genes.

How many of the descriptions are different as a result of this transfer? (same method)
```{r echo=F}
df=data.frame(T10=desc[shared,"description"],
              Ara=AraBedU[shared,"description"])
df$Ara=as.character(df$Ara)
df$T10=as.character(df$T10)
df$isSame=df$T10==df$Ara
t=table(df$isSame)
```
Transferring the descriptions from `r descFile` changed field 14 for `r pn(t["FALSE"])` genes.

## Write a new bed file
```{r echo=F}
test1 = nrow(newBed)==nrow(AraBed)
test2 = length(unique(newBed$gene))==length(unique(AraBed$gene))
testForComment = test1 & test2
isGoodComment = "As expected, this is the same number of rows and genes we started with."
isBadComment = "That's different from what we started with, so something is wrong."
ifComment = ifelse(testForComment, isGoodComment, isBadComment)
```

The new version of the file will contain `r pn(nrow(newBed))` rows, describing `r pn(length(unique(newBed$gene)))` genes. `r ifComment`

Save the new version of the bed file in the results folder.
```{r}
fnameOut = "results/Araport11.bed"
write.table(newBed[bednames], fnameOut, sep='\t',row.names=F,
            col.names=F,quote=F)
system(paste("gzip -f", fnameOut))
```


# Conclusions

We used the bed detail fields from `r descFile` to replace those fields in `r fnameAra` (except for the genes that did not have values in `r descFile`) and saved the results as `r fnameOut`


**SessionInfo**
```{r}
sessionInfo()
```
