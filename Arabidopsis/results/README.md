## About these files

* genedescr.tsv.gz - made using Descriptions.Rmd.

Reports gene symbol and curator summary for every annotated Arabidopsis gene in TAIR10. 