#!/usr/bin/env python

ex=\
"""
Fix gene description file downloaded from TAIR Web site.
Insert NA for empty fields. Eliminate empty lines.
Reads data from stdin, writes to stdout.
"""

import sys,optparse,fileinput,re

sep='\t'
agi_regex=re.compile('AT[1-5CM]G[0-9]{5}')

def main():
    numFields=0
    lineNum=0
    agis={}
    for line in fileinput.input():
        lineNum=lineNum+1
        if lineNum==1:
            heads=line.rstrip().split('\t')
            numFields=len(heads)
            sys.stdout.write(sep.join(heads)+'\n')
            sys.stderr.write("There were %i fields.\n"%numFields)
        else:
            line=line.rstrip('\n')
            if line.rstrip()=='':
                continue
            # there's an extra tab on the end
            toks=line.split('\t')
            if len(toks)!=numFields+1: 
                raise ValueError("Wrong number of fields on line: %i"%lineNum)
            toks=toks[0:numFields]
            for i in range(0,len(toks)):
                if toks[i]=='':
                    toks[i]='NA'
            if not agi_regex.match(toks[0]):
                raise ValueError('%s not AGI code'%toks[0])
            if not agis.has_key(toks[0]):
                agis[toks[0]]=toks
            else:
                raise ValueError("Too many lines for: %s"%toks[0])
            sys.stdout.write(sep.join(toks)+'\n')
    sys.stderr.write("Wrote data for %i %ss\n"%(len(agis.keys()),heads[0]))

if __name__=='__main__':
    usage='%prog\n'+ex
    parser=optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main()
